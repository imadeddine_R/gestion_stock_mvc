package com.stock.mvc.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Vente implements Serializable{
	

		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		//@Column(name = "idVente")
		private Long idVente;
		
		private String code;
		
		@Temporal(TemporalType.TIMESTAMP)
		private Date dateVente;
		
		@OneToMany
		@JoinColumn(name ="vente")
		private List<LigneVente> ligneVentes;

		public Vente(){
		}
		public Long getIdVente() {
			return idVente;
		}

		public void setIdVente(Long idVente) {
			this.idVente = idVente;
		}
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public Date getDateVente() {
			return dateVente;
		}
		public void setDateVente(Date dateVente) {
			this.dateVente = dateVente;
		}
		public List<LigneVente> getLigneVentes() {
			return ligneVentes;
		}
		public void setLigneVentes(List<LigneVente> ligneVentes) {
			this.ligneVentes = ligneVentes;
		}
}
