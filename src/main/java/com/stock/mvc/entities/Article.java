package com.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
//@Table(name = "article")
public class Article implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	//@Column(name = "idArticle")
	private Long idArticle;

	private String codeArticle;
	
	private String designation;
	
	private BigDecimal prixUnitaiteHT;
	
	private BigDecimal tauxTva;
	
	private BigDecimal prixUnitaiteHTC;
	
	private String photo;
	
	@ManyToOne //plusieur article dans une categorie
	@JoinColumn (name = "idCategorie")
	private Categorie category;
	
	public Article(){
	}
	public Long getIdArticle() {
		return idArticle;
	}

	public void setIdArticle(Long idArticle) {
		this.idArticle = idArticle;
	}

	public String getCodeArticle() {
		return codeArticle;
	}

	public void setCodeArticle(String codeArticle) {
		this.codeArticle = codeArticle;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public BigDecimal getPrixUnitaiteHT() {
		return prixUnitaiteHT;
	}

	public void setPrixUnitaiteHT(BigDecimal prixUnitaiteHT) {
		this.prixUnitaiteHT = prixUnitaiteHT;
	}

	public BigDecimal getTauxTva() {
		return tauxTva;
	}

	public void setTauxTva(BigDecimal tauxTva) {
		this.tauxTva = tauxTva;
	}

	public BigDecimal getPrixUnitaiteHTC() {
		return prixUnitaiteHTC;
	}

	public void setPrixUnitaiteHTC(BigDecimal prixUnitaiteHTC) {
		this.prixUnitaiteHTC = prixUnitaiteHTC;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Categorie getCategory() {
		return category;
	}

	public void setCategory(Categorie category) {
		this.category = category;
	}
}
