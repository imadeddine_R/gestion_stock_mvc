package com.stock.mvc.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.stock.mvc.entities.Categorie;
import com.stock.mvc.services.ICategorieService;

@Controller
@RequestMapping(value ="/categorie")
public class CategoerieController {

	@Autowired // pour faire l'injection automatique
	private ICategorieService categorieservice;

	@RequestMapping(value ="/" )
	public String categorie(Model model) {
		List<Categorie> categories = categorieservice.selectAll();
		if(categories == null){
			categories = new ArrayList<Categorie>();
		}
		model.addAttribute("categories",categories);
		return "categorie/categorie";
	}

	@RequestMapping(value="/nouveau", method= RequestMethod.GET)
	public String ajoutercategorie(Model model) {
		Categorie categorie = new Categorie();
		model.addAttribute("categorie", categorie);
		return "categorie/ajouterCategorie";
	}

	@RequestMapping(value = "/enregistrer")
	public String enregistrercategorie(Model model, Categorie categorie, MultipartFile file) {
		
		if(categorie.getIdCategorie() !=null) {
			categorieservice.update(categorie);
		}else {
			categorieservice.save(categorie);
		}	
		return "redirect:/categorie/";
	}
	
	@RequestMapping(value="/modifier/{Idcategorie}", method= RequestMethod.GET)
	public String modifiercategorie(Model model,@PathVariable Long Idcategorie) {
		if(Idcategorie!=null) {
			Categorie categorie=categorieservice.getById(Idcategorie);
			if(categorie !=null) {
				model.addAttribute("categorie", categorie);
			}
		}
		return "categorie/ajouterCategorie";
	}
	@RequestMapping(value="/supprimer/{Idcategorie}", method= RequestMethod.GET)
	public String supprimercategorie(Model model,@PathVariable Long Idcategorie) {
		if(Idcategorie!=null) {
			Categorie categorie=categorieservice.getById(Idcategorie);
			if(categorie !=null) {
				model.addAttribute("categorie", categorie);
				categorieservice.remove(Idcategorie);
			}
		}
		return "redirect:/categorie/";
	}
}
