package com.stock.mvc.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.stock.mvc.entities.Article;
import com.stock.mvc.entities.Categorie;
import com.stock.mvc.services.IArticleService;
import com.stock.mvc.services.ICategorieService;
import com.stock.mvc.services.IFlickrService;

@Controller
@RequestMapping(value ="/article")
public class ArticleController {

	@Autowired // pour faire l'injection automatique
	private IArticleService articleservice;
	
	@Autowired
	private ICategorieService categorieservice;

	@Autowired
	private IFlickrService flickrService;

	@RequestMapping(value ="/" )
	public String article(Model model) {
		List<Article> articles = articleservice.selectAll();
		if(articles == null){
			articles = new ArrayList<Article>();
		}
		model.addAttribute("articles",articles);
		return "article/article";
	}

	@RequestMapping(value="/nouveau", method= RequestMethod.GET)
	public String ajouterarticle(Model model) {
		Article article = new Article();
		List<Categorie> categories = categorieservice.selectAll();
		if(categories == null) {
			categories = new ArrayList<Categorie>();
		}
		model.addAttribute("article", article);
		model.addAttribute("categories", categories);
		System.out.println("je suis la");
		return "article/ajouterarticle";
	}

	@RequestMapping(value = "/enregistrer")
	public String enregistrerarticle(Model model, Article article, MultipartFile file) {
		String photoUrl= null;
		if(article !=null) {
			if(file != null && !file.isEmpty()) {
				InputStream stream=null;		
				try {
					stream = file.getInputStream();
					photoUrl = flickrService.savePhoto(stream, article.getCodeArticle());
					article.setPhoto(photoUrl);
				} catch (Exception e) {
					e.printStackTrace();
				}finally {
					try {
						stream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		if(article.getIdArticle() !=null) {
			articleservice.update(article);
		}else {
			articleservice.save(article);
		}	
		return "redirect:/article/";
	}
	
	@RequestMapping(value="/modifier/{Idarticle}", method= RequestMethod.GET)
	public String modifierarticle(Model model,@PathVariable Long Idarticle) {
		System.out.println("je suis la");
		if(Idarticle!=null) {
			Article article = articleservice.getById(Idarticle);
			List<Categorie> categories = categorieservice.selectAll();
			if(categories == null) {
				categories = new ArrayList<Categorie>();
			}
			model.addAttribute("categories", categories);
			if(article != null) {
				model.addAttribute("article", article);
			}
		}
		
		return "article/ajouterarticle";
	}
	@RequestMapping(value="/supprimer/{Idarticle}", method= RequestMethod.GET)
	public String supprimerarticle(Model model,@PathVariable Long Idarticle) {
		if(Idarticle!=null) {
			Article article=articleservice.getById(Idarticle);
			if(article !=null) {
				model.addAttribute("article", article);
				articleservice.remove(Idarticle);
			}
		}
		return "redirect:/article/";
	}
}
