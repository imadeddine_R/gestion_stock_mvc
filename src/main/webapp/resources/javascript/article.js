$(document).ready(function (){
	$("#tauxTva").on("keyup",function(){
		tvaKeyUpFunction();
	});
	
});


tvaKeyUpFunction = function(){
	var prixUnitaireHT = $("#prixUnitaiteHT").val();
	var tauxTva = $("#tauxTva").val();
	var prixUnitaireTTC = parseFloat(prixUnitaireHT) * parseFloat(tauxTva) / 100 + parseFloat(prixUnitaireHT);
	$("#prixUnitaiteTTC").val(prixUnitaireTTC);
}